+++
draft = false
comments = false
slug = ""
tags = []
categories = ["about"]
title = "About"

showpagemeta = false
showcomments = false
+++

* As of 2021, I'm a research analyst at the [Center on Long-Term Risk](https://longtermrisk.org/). My research interests include coopeation and conflict between advanced AIs.
* From 2017 to 2020 I was a postdoc in computational theoretical physics in Toulouse (south of France).
* From 2014 to 2017, I was a PhD in theoretical physics in Paris, France, under supervision of [Anuradha Jagannathan](https://www.equipes.lps.u-psud.fr/Jagannathan/).

I play the [clarinet](https://youtu.be/-Oqrw93E1_g), the [soprano sax](https://youtu.be/FLvJWxeZJH0) and various [recorders](https://youtu.be/qnIB_BDPU_w) *(click on each instrument for a piece I like)*.
