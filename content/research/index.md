+++
draft = false
comments = false
slug = ""
tags = []
categories = ["research"]
title = "Research"

showpagemeta = false
showcomments = false
+++

### Cooperation and conflict between AIs
Sufficently advanced artificial intelligence may significantly impact the future of the world.
I'm interested in thinking about how advanced AI could cause a lot of harm, in particular through large-scale conflict.
If you'd like to know more about this theme please feel free to [contact me](/contact).
You can also have a look at this [research agenda](https://longtermrisk.org/research-agenda).

### Past research in physics
*While I'm no longer working on those themes, I'm always happy to discuss them. Feel free to reach out.*
#### Why are humans not quantum? Thermalization and many-body localization
How do the classical laws of thermodynamics, predicting the behaviour of macroscopic systems, emerge from the underlying quantum dynamics of their microscopic constituents?
Answering this question is not simple, for at least two reasons.

1. Thermalization, the process by which a system becomes effectively classical, is accompanied at the microscopic level by the fast increase of entanglement entropy.
This hinders numerical simulations, as their complexity increases exponentially with entanglement entropy.
2. No all quantum systems thermalize. First, a handful of systems escape thermalization by being [quantum integrable](https://en.wikipedia.org/wiki/Integrable_system#Quantum_integrable_systems).
More generically, spatial disorder, by generating destructive interferences, can arrest transport and prevent thermalization. This is the well-known phenomenon of many-body localization (MBL).

I worked on characterizing the phase transition between the thermal phase, where the system at large time and length scales becomes effectively classical, and the MBL phase where the system keeps its quantum coherence.
I contributed to this topic by (i) developping numerical methods for the quantum simulation of large systems, (ii) employing these methods to probe the physics phase transition.

#### Quasicrystals
Quasicrystals are metallic alloys whose atoms arrange in a non-periodic yet highly ordered fashion.
They are notably famous for being highly symmetric under rotation.
Owing to their non-periodicity, mathematical models of quasicrystals feature some unusual mathematical beasts: the electronic density can be a scale-invariant [multifractal object](https://en.wikipedia.org/wiki/Multifractal_system), and the integrated density of states can be a [Devil's staircase](https://en.wikipedia.org/wiki/Cantor_function).
During my PhD, I tried to characterise the weird mathematical properties of quasicrystals using some simple models mathematical models.
