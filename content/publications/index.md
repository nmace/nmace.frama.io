+++
draft = false
comments = false
slug = ""
tags = []
categories = ["publications"]
title = "Publications"

showpagemeta = false
showcomments = false
+++

See my preprints on [arXiv.org](https://arxiv.org/a/mace_n_1.html), or on [Google Scholar](https://scholar.google.fr/citations?user=Qi6hqOEAAAAJ&hl=en).
My ORCID is [0000-0002-2438-7524](https://orcid.org/0000-0002-2438-7524).

2024
----
+ **Safe Pareto Improvements for Expected Utility Maximizers in Program Games**  
A. DiGiovanni, J. Clifton, N. Macé  
[arXiv:2403.05103](https://arxiv.org/abs/2403.05103)

2022
----
+ **Superfluidity vs thermalisation in a nonlinear Floquet system**  
S. Mu, N. Macé, J. Gong, C. Miniatura, G. Lemarié, M. Albert  
[arXiv:2207.06951](https://arxiv.org/abs/2207.06951)  

+ **Evolutionary Stability of Other-Regarding Preferences Under Complexity Costs**  
A. DiGiovanni, N. Macé, J. Clifton  
[arXiv:2207.03178](https://arxiv.org/abs/2207.03178)  

+ **Many-body localization stabilizes topological order in random interacting Ising/Majorana chains**  
N. Laflorencie, G. Lemarié, N. Macé  
[arXiv:2201.00556](https://arxiv.org/abs/2201.00556)  

+ **Probing symmetries of quantum many-body systems through gap ratio statistics**  
O. Giraud, N. Macé, E. Vernier, F. Alet   
[*Phys. Rev. X* **12**, 011006, (2022)](https://link.aps.org/doi/10.1103/PhysRevX.12.011006) [arXiv:2008.11173](https://arxiv.org/abs/2008.11173)  

2020
----
+ **Chain breaking and Kosterlitz-Thouless scaling at the many-body localization transition**  
N. Laflorencie, G. Lemarié, N. Macé   
[*Phys. Rev. Research* **2**, 042033, (2020)](https://link.aps.org/doi/10.1103/PhysRevResearch.2.042033) [arXiv:2004.02861](https://arxiv.org/abs/2004.02861)  

2019
----
+ **Quantum circuit at criticality**  
N. Macé   
[arXiv:1912.09489](https://arxiv.org/abs/1912.09489)  

+ **Multifractal scalings across the many-body localization transition**  
N. Macé, F. Alet and N. Laflorencie  
[*Phys. Rev. Lett.* **123**, 180601 (2019)](https://link.aps.org/doi/10.1103/PhysRevLett.123.180601) [arXiv:1812.10283](https://arxiv.org/abs/1812.10283)  

+ **From eigenstate to Hamiltonian: new prospects for ergodicity and localization**  
M. Dupont, N. Macé and N. Laflorencie  
[*Phys. Rev. B* **100**, 134201 (2019)](https://link.aps.org/doi/10.1103/PhysRevB.100.134201) [arXiv:1907.12124](https://arxiv.org/abs/1907.12124)  

+ **Many-body localization in a quasiperiodic Fibonacci chain**  
N. Macé, N. Laflorencie and F. Alet  
[*SciPost Phys.* **6**, 050 (2019)](https://scipost.org/SciPostPhys.6.4.050) [arXiv:1811.01912](https://arxiv.org/abs/1811.01912)  


2018
----

+ **Shift-invert diagonalization of large many-body localizing spin chains**  
F. Pietracaprina, N. Macé, D. J. Luitz and F. Alet  
[*SciPost Phys.* **5**, 045 (2018)](https://scipost.org/submissions/1803.05395v3/) [arXiv:1803.05395](https://arxiv.org/abs/1803.05395)  
[Open access, source code available ([link](https://bitbucket.org/dluitz/sinvert_mbl/))]

2017
----
+ **Critical eigenstates and their properties in one- and two-dimensional quasicrystals**  
N. Macé, A. Jagannathan, P. Kalugin, R. Mosseri et F. Piéchon  
[*Phys. Rev. B* **96**, 045138 (2017)](https://link.aps.org/doi/10.1103/PhysRevB.96.045138) [arXiv:1706.06796](https://arxiv.org/abs/1706.06796)

+ **Gap structure of 1D cut and project Hamiltonians**  
N. Macé, A. Jagannathan and F. Piéchon  
[*J. Phys.: Conference Series* **809**(1) (2017)](http://stacks.iop.org/1742-6596/809/i=1/a=012023), [arXiv:1704.06463](https://arxiv.org/abs/1704.06463)  
[Open access, conference proceeding]

2016
----
+ **Quantum simulation of a 2D quasicrystal with cold atoms**  
N. Macé, A. Jagannathan and M. Duneau  
[*Crystals* **6(10)** (2016)](https://www.mdpi.com/2073-4352/6/10/124), [arXiv:1609.08509](https://arxiv.org/abs/1609.08509)  
[Open access, Review]

+ **Fractal dimensions of the wavefunctions and local spectral measures on the Fibonacci chain**  
N. Macé, A. Jagannathan and F. Piéchon  
[*Phys. Rev. B* **93**, 205153 (2016)](https://journals.aps.org/prb/abstract/10.1103/PhysRevB.93.205153), [arXiv:1601.00532](https://arxiv.org/abs/1601.00532)

PhD Thesis
----
**Electronic properties of quasicrystals**  
[link](https://framagit.org/Yukee/PhD_thesis/raw/master/main.pdf)
